import service from '@/libs/service';

let admin = {};

admin.list = function (query, page, back) {
    var body = {
        'userQuery': query,
        'page': page
    };
    service.postBody('/manage/system/admin/list', body, back);
};

admin.addOrUpdate = function (bean, roleIds, back) {
    var body = {'user': bean, 'roleIds': roleIds};
    service.postBody('/manage/system/admin/addOrUpdate', body, back);
};

admin.get = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/system/admin/get', body, back);
};

admin.delete = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/system/admin/delete', body, back);
};

admin.isExist = function (id, account, back) {
    var body = {
        'id': id,
        'account': account
    };
    service.postBody('/manage/system/admin/isExist', body, back);
};

admin.getInfo = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/system/admin/getInfo', body, back);
};

admin.updatePassword = function (id, password, back) {
    var body = {
        'id': id,
        'password': password
    };
    service.postBody('/manage/system/admin/updatePassword', body, back);
};

admin.toGeneral = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/system/admin/toGeneral', body, back);
};
export default admin;
