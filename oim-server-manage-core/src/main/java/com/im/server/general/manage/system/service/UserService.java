package com.im.server.general.manage.system.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.im.server.general.common.bean.User;
import com.im.server.general.common.bean.UserNumber;
import com.im.server.general.common.bean.system.UserRole;
import com.im.server.general.common.dao.NumberDAO;
import com.im.server.general.common.dao.UserDAO;
import com.im.server.general.common.dao.system.UserRoleDAO;
import com.im.server.general.common.data.system.UserInfo;
import com.im.server.general.common.data.system.UserInfoQuery;
import com.im.server.general.common.data.system.UserRoleInfo;
import com.onlyxiahui.common.util.OnlyDateUtil;
import com.onlyxiahui.common.util.OnlyMD5Util;
import com.onlyxiahui.query.hibernate.QueryWrapper;
import com.onlyxiahui.query.hibernate.util.QueryUtil;
import com.onlyxiahui.query.page.QueryPage;

/**
 * 
 * date 2018-07-17 22:39:50<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@Service
@Transactional
public class UserService {

	@Resource
	UserDAO userDAO;
	@Resource
	UserRoleDAO userRoleDAO;
	@Resource
	private NumberDAO numberDAO;
	/**
	 * 跟据id获取用户实体
	 * 
	 * @param id
	 * @return
	 */
	public User getById(String id) {
		return userDAO.get(id);
	}

	/**
	 * 根据id是否存在执行新增或者修改
	 * 
	 * @param user
	 */

	public void addOrUpdate(User user, List<String> roleIds) {
		addOrUpdate(user);
		addOrUpdate(user.getId(), roleIds);
	}

	public void addOrUpdate(User user) {
		if (null == user.getId() || "".equals(user.getId())) {
			int i = new Random().nextInt(20);
			i = i + 1;
			UserNumber userNumber = new UserNumber();// 生成数子账号
			userNumber.setCreateTime(OnlyDateUtil.getNowDate());

			numberDAO.save(userNumber);

			user.setNumber(userNumber.getId());
			user.setHead(i + "");
			user.setCreateTime(OnlyDateUtil.getNowDate());
			user.setPassword(OnlyMD5Util.md5L32(user.getPassword()));
			userDAO.save(user);
		} else {
			user.setUpdateTime(new Date());
			userDAO.updateSelective(user);
		}
	}

	public void addOrUpdate(String userId, List<String> roleIds) {
		List<UserRole> rmList = userRoleDAO.getListByUserId(userId);

		List<UserRole> deleteList = new ArrayList<UserRole>();

		Map<String, UserRole> map = new HashMap<String, UserRole>();
		for (UserRole ur : rmList) {
			if (map.containsKey(ur.getRoleId())) {
				deleteList.add(ur);
			}
			map.put(ur.getRoleId(), ur);
		}

		List<UserRole> addList = new ArrayList<UserRole>();
		for (String roleId : roleIds) {
			UserRole rm = map.remove(roleId);
			if (null == rm) {
				rm = new UserRole();
				rm.setRoleId(roleId);
				rm.setUserId(userId);
				addList.add(rm);
			}
		}
		deleteList.addAll(map.values());
		if (!addList.isEmpty()) {
			for (UserRole ur : addList) {
				userRoleDAO.save(ur);
			}
		}

		if (!deleteList.isEmpty()) {
			for (UserRole ur : deleteList) {
				userRoleDAO.delete(ur.getId());
			}
		}
	}

	/**
	 * 查询用户()
	 * 
	 * @param user
	 * @param page
	 * @return
	 */
	public List<UserInfo> queryUserDataList(UserInfoQuery userQuery, QueryPage queryPage) {
		QueryWrapper queryMap = QueryUtil.getQueryWrapper(userQuery);
		queryMap.put("orderBy", " order by createTime desc");

		queryMap.setPage(queryPage);
		List<UserInfo> list = userDAO.queryList(userQuery, queryPage, UserInfo.class);

		List<String> userIdList = new ArrayList<String>();
		Map<String, UserInfo> map = new HashMap<String, UserInfo>();
		for (UserInfo ud : list) {
			userIdList.add(ud.getId());
			map.put(ud.getId(), ud);
		}
		List<UserRoleInfo> urs = userRoleDAO.getUserRoleInfoListByUserIdList(userIdList);

		for (UserRoleInfo ur : urs) {
			UserInfo ud = map.get(ur.getUserId());
			if (null != ud) {
				List<UserRoleInfo> userRoleList = ud.getUserRoleInfoList();
				if (null == userRoleList) {
					userRoleList = new ArrayList<UserRoleInfo>();
					ud.setUserRoleInfoList(userRoleList);
				}
				userRoleList.add(ur);
			}
		}
		return list;
	}

	/**
	 * 根据id删除用户
	 * 
	 * @param id
	 */
	public void delete(String id) {
		userDAO.delete(id);
		userRoleDAO.deleteByUserId(id);
	}

	public User getUserByAccount(String account) {
		return userDAO.getUserByAccount(account);
	}

	public boolean isExistAccount(String account, String id) {
		boolean isExist = true;
		User data = userDAO.getUserByAccount(account);
		if (null == data || data.getId().equals(id)) {
			isExist = false;
		}
		return isExist;
	}

	public boolean updatePassword(String userId, String password) {
		password = OnlyMD5Util.md5L32(password);
		boolean mark = userDAO.updatePassword(userId, password);
		return mark;
	}
	
	/**
	 * 修改用户类型
	 * @param userId
	 * @param type
	 * @return
	 */
	public boolean updateType(String userId, String type) {
		boolean mark = userDAO.updateType(userId, type);
		return mark;
	}

	/**
	 * 获取系统用户的角色列表 date 2018-07-18 09:29:20<br>
	 * description
	 * 
	 * @author XiaHui<br>
	 * @param userId
	 * @return
	 * @since
	 */
	public List<UserRoleInfo> getUserRoleInfoListByUserId(String userId) {
		List<String> userIdList = new ArrayList<String>();
		userIdList.add(userId);
		List<UserRoleInfo> urs = userRoleDAO.getUserRoleInfoListByUserIdList(userIdList);
		return urs;
	}
}
