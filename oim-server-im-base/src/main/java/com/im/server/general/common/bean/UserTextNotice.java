package com.im.server.general.common.bean;

import java.util.Date;

import javax.persistence.Entity;

import com.im.base.bean.BaseBean;

/**
 * date 2016-01-04 21:18:55<br>
 * 用户收到通知的记录表，关联通知表。
 * @author XiaHui
 */
@Entity(name = "im_user_text_notice")
public class UserTextNotice extends BaseBean {

	private String userId;//接受通知的用户id
	private String textNoticeId;//通知表的id
	private Date createTime;// 建立时间
	private String isRead;//是否已读 0:false/未读 1:true/已读

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTextNoticeId() {
		return textNoticeId;
	}

	public void setTextNoticeId(String textNoticeId) {
		this.textNoticeId = textNoticeId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getIsRead() {
		return isRead;
	}

	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}

}
