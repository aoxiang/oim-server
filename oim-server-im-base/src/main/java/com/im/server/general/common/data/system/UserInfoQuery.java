package com.im.server.general.common.data.system;

import java.util.Date;

import com.onlyxiahui.im.message.data.query.UserQuery;

/**
 * date 2018-07-12 10:22:36<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
public class UserInfoQuery extends UserQuery {

	private Date startCreateTime;// 建立时间
	private Date endCreateTime;//
	private String flag;// 有效标志 1：启用 0：停用
	private String mobile;// 手机
	private String email;// 电子信箱
	private String qq;// qq
	private String identityCard;// 身份证号码
	private String remark;// 备注
	private String introduce;// 个人简介
	private String type;// 用户类型 0：普通用户 1：普通管理员 2：超级管理员

	public Date getStartCreateTime() {
		return startCreateTime;
	}

	public void setStartCreateTime(Date startCreateTime) {
		this.startCreateTime = startCreateTime;
	}

	public Date getEndCreateTime() {
		return endCreateTime;
	}

	public void setEndCreateTime(Date endCreateTime) {
		this.endCreateTime = endCreateTime;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getIdentityCard() {
		return identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
